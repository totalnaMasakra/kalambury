package kalambury_cli;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;

class ConnectWindow extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	private GridBagLayout layout;
	private GridBagConstraints constr;
	
	private JLabel hostLabel, portLabel, userLabel;
	private JTextField hostField, userField;

	private JSpinner portField;
	private JButton connectButton, cancelButton;
	
	final int xSize, ySize;
	
	String srvHost, userName;
	Integer srvPort;
	boolean isReady;
	
	public String getHost(){ return srvHost; }
	public Integer getPort(){ return srvPort; }
	public String getUser(){ return userName; }
	public boolean isReady(){ return isReady; }
	
	public ConnectWindow( JFrame parent, String srvHost_, Integer srvPort_, String userName_ ) {
		super( parent, "Połącz", Dialog.ModalityType.APPLICATION_MODAL );
		
		srvHost = srvHost_;
		srvPort = srvPort_;
		userName = userName_;
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		layout = new GridBagLayout();
		setLayout(layout);
		constr = new GridBagConstraints();

		hostLabel = new JLabel("Adres serwera");
        constr.fill = GridBagConstraints.BOTH;
        constr.gridx = 0;
        constr.gridy = 0;
        constr.gridwidth = 1;
        constr.gridheight = 1;
        constr.weightx = 1 ;
        constr.weighty = 1 ;
        constr.ipadx = 0;
        constr.ipady = 0;
        constr.insets = new Insets(6,6,6,6);
        add(hostLabel, constr);

		portLabel = new JLabel("Port");
        constr.fill = GridBagConstraints.BOTH;
        constr.gridx = 0;
        constr.gridy = 1;
        add(portLabel, constr);
        
		userLabel = new JLabel("Nazwa użytkownika");
        constr.fill = GridBagConstraints.BOTH;
        constr.gridx = 0;
        constr.gridy = 2;
        add(userLabel, constr);

		hostField = new JTextField( srvHost );
        constr.fill = GridBagConstraints.BOTH;
        constr.gridx = 1;
        constr.gridy = 0;
        add(hostField, constr);

		portField = new JSpinner();
		portField.setValue(srvPort);
        constr.fill = GridBagConstraints.BOTH;
        constr.gridx = 1;
        constr.gridy = 1;
        add(portField, constr);

		userField = new JTextField( userName );
        constr.fill = GridBagConstraints.BOTH;
        constr.gridx = 1;
        constr.gridy = 2;
        add(userField, constr);
        
		connectButton = new JButton( "Połącz" );
        connectButton.addActionListener(this);
        connectButton.setActionCommand("connect");
        constr.fill = GridBagConstraints.BOTH;
        constr.gridx = 0;
        constr.gridy = 3;
        constr.insets = new Insets(12,12,12,12);
        add(connectButton, constr);
        
        cancelButton = new JButton( "Anuluj" );
        cancelButton.addActionListener(this);
        cancelButton.setActionCommand("cancel");
        constr.fill = GridBagConstraints.BOTH;
        constr.gridx = 1;
        constr.gridy = 3;
        constr.insets = new Insets(12,12,12,12);
        add(cancelButton, constr);
        
        xSize = 300;
        ySize = 200;

        setPreferredSize( new Dimension( xSize, ySize ) );
        setMinimumSize( new Dimension( xSize, ySize ) );
        setMaximumSize( new Dimension( xSize, ySize ));
        pack();
        
	}

	@Override
	public void actionPerformed(ActionEvent a) {

		switch( a.getActionCommand() ){
		case "connect":
			srvHost=hostField.getText();
			srvPort=(Integer) portField.getValue();
			userName=userField.getText();
			isReady=true;
			dispose();
			break;
		case "cancel":
			isReady=false;
			dispose();
			break;
		}
		
	}
}
