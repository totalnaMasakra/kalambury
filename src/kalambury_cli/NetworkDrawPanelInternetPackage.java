package kalambury_cli;

import java.awt.Color;
import java.io.Serializable;

public class NetworkDrawPanelInternetPackage implements Serializable {
	
	private static final long serialVersionUID = 153L;
	
	private int x;
	private int y;
	private int s;
	private Color c;
	
	public NetworkDrawPanelInternetPackage( int x, int y, int s, Color c )
	{ this.x=x; this.y=y; this.s=s; this.c=c; }
	
	public int getX() { return x; }
	public int getY() { return y; }
	public int getSize() { return s; }
	public Color getColor() { return c; }
	
}