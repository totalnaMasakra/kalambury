package kalambury_cli;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultCaret;
import javax.swing.text.html.HTMLEditorKit;

import kalambury_srv.ControlProtocol;

public class MainWindow extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JMenuBar	menuBar;
	private JMenu		menuBar_gra;
	private JMenuItem	menuBar_gra_polacz;
	private JMenuItem	menuBar_gra_rozlacz;
	private JMenuItem	menuBar_gra_statystyki;
	private JMenuItem	menuBar_gra_zakoncz;
	private JMenu		menuBar_pomoc;
	private JMenuItem	menuBar_pomoc_oProgramie;

	private GridBagLayout layout;
	private GridBagConstraints constr;

	private NetworkDrawPanel drawPanel;
	private JSpinner brushSize;
	private JComboBox<String> brushColor;
	private JTextPane chat;
	private JScrollPane chatScroll;
	private JTextField sendChat;
	private JButton keywordButton;
	private JLabel statusBar;

	private String srvHost, userName;
	private Integer srvPort;
	private Socket socket;
	private ServerTalker server;

	public MainWindow() {

		srvHost="localhost";
		userName="";
		srvPort= 2033;

		setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		setTitle("Kalambury");

		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(WindowEvent winEvt) {
				disconnect();
				System.exit(0);
			}
		});

		menuBar = new JMenuBar();

		menuBar_gra = new JMenu("Gra");
		menuBar.add(menuBar_gra);

		menuBar_gra_polacz = new JMenuItem("Połącz");
		menuBar_gra_polacz.addActionListener(this);
		menuBar_gra_polacz.setActionCommand("open connect dialog");
		menuBar_gra.add( menuBar_gra_polacz );

		menuBar_gra_rozlacz = new JMenuItem("Rozłącz");
		menuBar_gra_rozlacz.addActionListener(this);
		menuBar_gra_rozlacz.setActionCommand("close connection");
		menuBar_gra_rozlacz.setEnabled(false);
		menuBar_gra.add( menuBar_gra_rozlacz );

		menuBar_gra.addSeparator();

		menuBar_gra_statystyki = new JMenuItem("Statystyki");
		menuBar_gra_statystyki.addActionListener(this);
		menuBar_gra_statystyki.setActionCommand("stats");
		menuBar_gra.add(menuBar_gra_statystyki);

		menuBar_gra.addSeparator();

		menuBar_gra_zakoncz = new JMenuItem("Zakończ");
		menuBar_gra_zakoncz.addActionListener(this);
		menuBar_gra_zakoncz.setActionCommand("close app");
		menuBar_gra.add(menuBar_gra_zakoncz);

		menuBar_pomoc = new JMenu("Pomoc");
		menuBar.add(menuBar_pomoc);
		menuBar_pomoc_oProgramie = new JMenuItem("O programie...");
		menuBar_pomoc_oProgramie.addActionListener(this);
		menuBar_pomoc_oProgramie.setActionCommand("about");
		menuBar_pomoc.add(menuBar_pomoc_oProgramie);


		setJMenuBar(menuBar);

		layout = new GridBagLayout();
		setLayout(layout);
		constr = new GridBagConstraints();

		drawPanel = new NetworkDrawPanel();
		constr.fill = GridBagConstraints.CENTER;
		constr.gridx = 0;
		constr.gridy = 0;
		constr.gridwidth = 3;
		constr.gridheight = 1;
		constr.weightx = 1 ;
		constr.weighty = 0 ;
		constr.ipadx = 0;
		constr.ipady = 0;
		constr.insets = new Insets(2,2,2,2);
		add(drawPanel, constr);

		brushSize = new JSpinner();
		brushSize.setValue(10);
		brushSize.setPreferredSize(new Dimension(40, 30));
		brushSize.addChangeListener( new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				drawPanel.setSize( (Integer)brushSize.getValue() );
			}
		});
		constr.fill = GridBagConstraints.CENTER;
		constr.gridx = 0;
		constr.gridy = 1;
		constr.gridwidth = 1;
		constr.gridheight = 1;
		constr.weightx = 1 ;
		constr.weighty = 0 ;
		constr.ipadx = 0;
		constr.ipady = 0;
		constr.insets = new Insets(8,4,8,4);
		add(brushSize, constr);


		String colors[] = { "Czarny", "Biały", "Czerwony", "Zielony", "Niebieski", "Żółty" };
		brushColor = new JComboBox<String>( colors );
		brushColor.setPreferredSize(new Dimension(100, 30));
		brushColor.addActionListener(this);
		brushColor.setActionCommand( "change color" );
		constr.fill = GridBagConstraints.CENTER;
		constr.gridx = 1;
		constr.gridy = 1;
		constr.gridwidth = 1;
		constr.gridheight = 1;
		constr.weightx = 1 ;
		constr.weighty = 0 ;
		constr.ipadx = 0;
		constr.ipady = 0;
		constr.insets = new Insets(8,4,8,4);
		add(brushColor, constr);

		chat = new JTextPane();
		chat.setContentType("text/html");
		chat.setText("<html>");
		chat.setEditorKit( new HTMLEditorKit() );
		DefaultCaret caret = (DefaultCaret)chat.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		constr.fill = GridBagConstraints.BOTH;
		constr.gridx = 4;
		constr.gridy = 0;
		constr.gridwidth = 1;
		constr.gridheight = 1;
		constr.weightx = 1 ;
		constr.weighty = 1 ;
		constr.ipadx = 0;
		constr.ipady = 0;
		constr.insets = new Insets(2,2,2,2);
		chatScroll = new JScrollPane( chat );
		chatScroll.setPreferredSize(new Dimension(180, 400));
		chatScroll.setMinimumSize(new Dimension(180, 400));
		add(chatScroll, constr);
		chatScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		chatScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		chat.setEditable(false);

		sendChat = new JTextField();
		sendChat.addActionListener(this);
		sendChat.setActionCommand("send chat message");
		sendChat.setEnabled(false);
		constr.fill = GridBagConstraints.HORIZONTAL;
		constr.gridx = 4;
		constr.gridy = 1;
		constr.gridwidth = 1;
		constr.gridheight = 1;
		constr.weightx = 0 ;
		constr.weighty = 0 ;
		constr.ipadx = 0;
		constr.ipady = 0;
		constr.insets = new Insets(8,2,4,2);
		add(sendChat, constr);

		keywordButton = new JButton("PASS");
		keywordButton.addActionListener(this);
		keywordButton.setActionCommand("PASS the turn");
		constr.fill = GridBagConstraints.HORIZONTAL;
		constr.gridx = 4;
		constr.gridy = 1;
		constr.gridwidth = 1;
		constr.gridheight = 1;
		constr.weightx = 0 ;
		constr.weighty = 0 ;
		constr.ipadx = 0;
		constr.ipady = 0;
		constr.insets = new Insets(2,2,2,2);
		add(keywordButton, constr);

		statusBar = new JLabel("");
		statusBar.setPreferredSize(new Dimension(300,30));
		//keywordButton.addActionListener(this);
		//statusBar.setActionCommand("Add");
		constr.fill = GridBagConstraints.HORIZONTAL;
		constr.gridx = 0;
		constr.gridy = 2;
		constr.gridwidth = 5;
		constr.gridheight = 1;
		constr.weightx = 0 ;
		constr.weighty = 0 ;
		constr.ipadx = 0;
		constr.ipady = 0;
		constr.insets = new Insets(2,2,2,2);
		add(statusBar, constr);

		setMode( ControlProtocol.OFFLINE );
		setPreferredSize( new Dimension( 800, 600 ) );
		setMinimumSize( new Dimension( 700, 600 ) );
		pack();

	}

	public void setMode( int mode ){
		switch( mode ) {
		case ControlProtocol.MASTER:
			sendChat.setVisible(false);
			sendChat.setEnabled(false);
			keywordButton.setVisible(true);
			keywordButton.setEnabled(true);
			drawPanel.setEnabled(true);
			brushSize.setEnabled(true);
			brushColor.setEnabled(true);
			drawPanel.clearImage();
			statusBar.setText("Rysujesz" );
			menuBar_gra_statystyki.setEnabled(true);
			break;
		case ControlProtocol.PROBLEM:
			sendChat.setVisible(true);
			sendChat.setEnabled(true);
			keywordButton.setVisible(false);
			keywordButton.setEnabled(false);
			drawPanel.setEnabled(false);
			brushSize.setEnabled(false);
			brushColor.setEnabled(false);
			drawPanel.clearImage();
			statusBar.setText("Spróbuj zgadnąć słowo i napisz je w chacie");
			menuBar_gra_statystyki.setEnabled(true);
			break;
		case ControlProtocol.RESTART:
			break;
		case ControlProtocol.WAITING:
			sendChat.setVisible(true);
			sendChat.setEnabled(false);
			keywordButton.setVisible(false);
			keywordButton.setEnabled(false);
			drawPanel.setEnabled(false);
			brushSize.setEnabled(false);
			brushColor.setEnabled(false);
			drawPanel.clearImage();
			statusBar.setText("Czekanie...");
			menuBar_gra_statystyki.setEnabled(false);
			break;
		case ControlProtocol.OFFLINE:
			sendChat.setVisible(true);
			sendChat.setEnabled(false);
			keywordButton.setVisible(false);
			keywordButton.setEnabled(false);
			drawPanel.setEnabled(false);
			brushSize.setEnabled(false);
			brushColor.setEnabled(false);
			drawPanel.clearImage();
			statusBar.setText("Rozłączono");
			chat.setText("<html>");
			menuBar_gra_statystyki.setEnabled(false);
			break;
		}
	}
	
	void showStats( ControlProtocol cp ){
		if( cp.getValue() != ControlProtocol.STATS ) return;
		JOptionPane.showMessageDialog(this , cp.getWord() ,"Statystyki" , JOptionPane.PLAIN_MESSAGE );
	}

	void connect(){
		try {
			final JFrame mw = this;
			ConnectWindow cw = new ConnectWindow( mw , srvHost, srvPort, userName );
			cw.setVisible(true);
			if( cw.isReady ) {
				srvHost = cw.getHost();
				srvPort = cw.getPort();
				userName = cw.getUser();
				socket = new Socket( srvHost, srvPort );

				PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
				out.println(userName);

				menuBar_gra_polacz.setEnabled(false);
				menuBar_gra_rozlacz.setEnabled(true);
				chat.setText("<html>");
				server = new ServerTalker( this, socket, chat.getStyledDocument() , drawPanel );
				server.start();
				drawPanel.setServerTalker(server);
				statusBar.setText( "Połączono z serwerem "+srvHost+" jako "+userName );
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog( this, "Nie udało się połączyć: "+e.getMessage(), "Błąd", JOptionPane.ERROR_MESSAGE );
			//e.printStackTrace();
		}

	}

	void disconnect(){
		try{
			menuBar_gra_polacz.setEnabled(true);
			menuBar_gra_rozlacz.setEnabled(false);
			setMode(ControlProtocol.OFFLINE);
			server.terminate();
			//server.stop();
			server=null;
			drawPanel.setServerTalker(null);
			socket.close();
		} catch(Exception e){}
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MainWindow mw = new MainWindow();
				mw.setVisible(true);
			}
		});
	}

	public void actionPerformed(ActionEvent a ) {



		switch( a.getActionCommand() )
		{

		case "send chat message":
			try {
				server.push(
						sendChat.getText() );
				sendChat.setText("");
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;

		case "change color":
			@SuppressWarnings("unchecked")
			String sel = ((JComboBox<String>)a.getSource()).getSelectedItem().toString();
			switch(sel){
			case "Czarny":		drawPanel.setColor(Color.black); break;
			case "Biały":		drawPanel.setColor(Color.white); break;
			case "Czerwony":	drawPanel.setColor(Color.red); break;
			case "Zielony":		drawPanel.setColor(Color.green); break;
			case "Niebieski":	drawPanel.setColor(Color.blue); break;
			case "Żółty":		drawPanel.setColor(Color.yellow); break;
			}
			break;

		case "PASS the turn":
			try {
				server.push( new ControlProtocol( ControlProtocol.RESTART , userName ) );
				setMode(ControlProtocol.RESTART);
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;

		case "open connect dialog":
			connect();
			break;

		case "close connection":
			disconnect();
			break;

		case "close app":
			dispose();
			break;

		case "stats":
			try {
				server.push( new ControlProtocol( ControlProtocol.STATS, userName ) );
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
		case "about":
			JOptionPane.showMessageDialog(this,
					"<html>By <span color=\"red\">Adam Klimaszewski</span> @2014 joł</html>",
					"O programie...",
					JOptionPane.PLAIN_MESSAGE);
			break;

		}

	}

}
