package kalambury_cli;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class NetworkDrawPanel extends JPanel {


	private static final long serialVersionUID = 1L;

	private int componentW = 500;
	private int componentH = 400;

	private BufferedImage image;
	private Graphics drawOnImage;

	private Color brushColor;
	private int brushSize = 10;
	private ServerTalker server = null;
	//private boolean sync = false;

	public void setColor( Color c ){ brushColor = c; }
	public void setSize( Integer i ) { brushSize = i; }
	public void setServerTalker( ServerTalker st ) { server = st; }
	//public void sync() { sync = true; }

	public NetworkDrawPanel() {

		setBorder(BorderFactory.createLineBorder(Color.black));

		setSize( new Dimension(componentW, componentH) );
		setMinimumSize( new Dimension(componentW, componentH) );
		setMaximumSize( new Dimension(componentW, componentH) );

		image = new BufferedImage(componentW, componentW, BufferedImage.TYPE_INT_RGB );
		drawOnImage = image.createGraphics();
		clearImage();

		brushColor = Color.BLACK;

		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if( isEnabled() ) sendPoint(e.getX(),e.getY());
			}
		});

		addMouseMotionListener(new MouseAdapter() {
			public void mouseDragged(MouseEvent e) {
				if( isEnabled() ) sendPoint(e.getX(),e.getY());
			}
		});
	}
	
	public void clearImage(){
		drawOnImage.setColor(Color.WHITE);
		drawOnImage.fillRect(0, 0, componentW, componentH);
		repaint();
	}

	public Dimension getPreferredSize() {
		return new Dimension(componentW,componentH);
	}

	private void sendPoint(int x, int y) {
		NetworkDrawPanelInternetPackage send;
		send = new NetworkDrawPanelInternetPackage( x, y, brushSize, brushColor );
		try {
			server.push( send );
		} catch (Exception e) {
			System.err.println("Nie wysłało punktu - " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void drawPoint( NetworkDrawPanelInternetPackage pkg ) {

		int x = pkg.getX();
		int y = pkg.getY();

		drawOnImage.setColor( pkg.getColor() );
		drawOnImage.fillOval( x - pkg.getSize() , y - pkg.getSize() , pkg.getSize()*2, pkg.getSize()*2 );
		repaint( x - pkg.getSize() , y - pkg.getSize() , pkg.getSize()*2, pkg.getSize()*2 );
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);       


		g.drawImage(image, 0, 0, null);

	}  

}
