package kalambury_cli;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

import kalambury_srv.ControlProtocol;

public class ServerTalker extends Thread {


	private ObjectOutputStream outToServer;
	private ObjectInputStream inFromServer;
	private StyledDocument chat;
	private NetworkDrawPanel img;
	private boolean running;
	private MainWindow parent;
	
	public ServerTalker( MainWindow parent, Socket socket, StyledDocument chat, NetworkDrawPanel img ) {
		running = true;
		this.parent = parent;
		try {
			outToServer = new ObjectOutputStream(socket.getOutputStream());
			inFromServer = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			System.err.println("Nie otwarto strumieni - błąd IO");
			running = false;
		}
		this.chat = chat;
		this.img = img;
	}

	public void run(){
		
		Object o;

		try {
			while ( ( o = inFromServer.readObject() ) != null && running==true ) {
				sendToObject( o );
			}
		} catch (ClassNotFoundException | IOException e) {}
		
		parent.disconnect();

	}

	private void sendToObject( Object o ){
		if( o instanceof String ){
			try {
					chat.insertString(chat.getLength(), (String)o , null );
			} catch (Exception e) { e.printStackTrace(); }
		}
		else if( o instanceof NetworkDrawPanelInternetPackage ){
			img.drawPoint( (NetworkDrawPanelInternetPackage)o );
		}
		else if( o instanceof ControlProtocol){
			ControlProtocol cp = (ControlProtocol)o;
			parent.setMode( cp.getValue() );
			if( cp.getValue() == ControlProtocol.MASTER && cp.getWord() != null )
				try { chat.insertString(chat.getLength(), "Hasło do narysowania, to "+cp.getWord()+"\n" , null ); }
			catch (BadLocationException e) {e.printStackTrace();}
			else if ( cp.getValue() == ControlProtocol.STATS && cp.getWord() != null )
				parent.showStats(cp);
		}
	}
	
	public void push( Object o ) throws IOException{
		outToServer.writeObject( o );
	}
	
	public void terminate(){ running = false; }

}