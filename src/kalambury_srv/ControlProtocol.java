package kalambury_srv;

import java.io.Serializable;

public class ControlProtocol implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final int value;
	private final String destUser;
	private final String word;
	
	public static final int MASTER = 111;
	public static final int PROBLEM = 222;
	public static final int OFFLINE = 333;
	public static final int WAITING = 432;
	public static final int STATS = 536;
	public static final int RESTART = 989;
	
	public ControlProtocol( int v ){
		value = v;
		destUser = null;
		word = null;
	}
	
	public ControlProtocol( int v, String du ) {
		value = v;
		destUser = du;
		word = null;
	}
	
	public ControlProtocol( int v, String du, String w) {
		value = v;
		destUser = du;
		word = w;
	}
	
	public int getValue() { return value; }
	public String getDestinationUser() { return destUser; }
	public String getWord() { return word; }

}
