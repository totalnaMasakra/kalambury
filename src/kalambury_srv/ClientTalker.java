package kalambury_srv;

import java.net.*;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;
import java.io.*;

class ClientTalker extends Thread {
	private Socket socket;
	private String clientName;
	private Vector<ClientTalker> clients;
	private LinkedBlockingQueue<Object> broadcast;
	private LinkedBlockingQueue<mcSession> session;
	
	private ObjectOutputStream outToClient;
	private ObjectInputStream inFromClient;
	
	private boolean running = true;
	
	private int points = 0;

	public ClientTalker( Socket arg0, Vector<ClientTalker> arg1, LinkedBlockingQueue<Object> arg2, LinkedBlockingQueue<mcSession> arg3 ) {
		socket = arg0;
		clients = arg1;
		broadcast = arg2;
		session = arg3;
	}
	
	public void run() {

		Object o;
		

		try { clientName = ( new BufferedReader( new InputStreamReader( socket.getInputStream() ) ) ).readLine(); }
		catch (IOException e) {
			System.err.println("Nie odczytano nazwy usera - błąd IO");
			return;
		}

		System.out.println("Połączono z " + clientName );

		try {
			outToClient = new ObjectOutputStream(socket.getOutputStream());
			inFromClient = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			System.err.println("Nie otwarto strumieni - błąd IO");
			return;
		}

		try {
			
			if( clientName.compareToIgnoreCase("") == 0 ){
				outToClient.writeObject( "Brak nicku, zrywanie połączenia" );
				close();
				push( new Object() );
				return;
			}
			for( ClientTalker ct : clients )
				if( clientName.compareToIgnoreCase( ct.getUserName() ) == 0 && this!=ct ){
					outToClient.writeObject( "Istnieje już user o podanym nicku, zrywanie połączenia" );
					close();
					push( new Object() );
					return;
				}
			
			
			
			broadcast.put(clientName+" zalogował się\n");
			while ( running ) {
				o = inFromClient.readObject(); 
				try {
					if( o instanceof String ) System.out.println( clientName+" przsyła wiadomość '"+ (String)o +"'" );
					else System.out.println("Otrzymano wiadomość od " + clientName );
					if ( o != null ) session.put( new mcSession(clientName, this, o) );
				}
				catch( InterruptedException e ){ System.err.println("Przerwanie ClientTalkera z userem " + clientName ); };
			}
		} catch (ClassNotFoundException | IOException | InterruptedException e) {
			System.err.println("Zakończenie ClientTalkera z " + clientName );
			}
		try { broadcast.put(clientName+" wylogowuje się\n"); } catch (Exception e) {
			System.err.println("Nie wysłano informacji o wylogowaniu usera " + clientName );
			}
		
		close();

	}
	
	public void close(){
		//try { session.put( new mcSession(clientName, this, null) );	}
		//catch (InterruptedException e) {}
		try {
			socket.close();
		} catch (IOException e) {}
		running = false;
	}
	
	public void push( Object o ) throws IOException{
		outToClient.writeObject( o );
	}
	
	public String getUserName(){
		return clientName;
	}
	
	public void addPoint() { points++; }
	public int getPoints() { return points; }
	
}
