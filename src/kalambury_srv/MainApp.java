package kalambury_srv;

import java.net.*;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;
import java.io.*;

public class MainApp {

	private static Vector<ClientTalker> clients;
	private static LinkedBlockingQueue<Object> broadcast;
	private static LinkedBlockingQueue<mcSession> session;
	private static ServerSocket socket;

	public static void main(String[] args) {

		int portNumber = 2033;

		try {
			socket = new ServerSocket(portNumber);
			clients = new Vector<ClientTalker>();
			broadcast = new LinkedBlockingQueue<Object>();
			session = new LinkedBlockingQueue<mcSession>();
			(new Broadcaster( clients, broadcast )).start();
			(new MasterOfCeremony( clients, broadcast, session )).start();
			System.out.println("Odpalono aplikację");
			while (true) {
				clients.add( new ClientTalker( socket.accept(), clients, broadcast, session ) );
				clients.lastElement().start();
			}
		} catch (IOException e) {
			System.err.println("Jebudu, nie zadziała to :P "+ e.getMessage());
			System.exit(-1);
		}
	}

}
