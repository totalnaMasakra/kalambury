package kalambury_srv;

import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;

class Broadcaster extends Thread {

	private Vector<ClientTalker> clients;
	private Vector<ClientTalker> clientsToDelete;
	private LinkedBlockingQueue<Object> broadcast;

	public Broadcaster( Vector<ClientTalker> arg0, LinkedBlockingQueue<Object> arg1 ) {
		clients = arg0;
		broadcast = arg1;
	}

	public void run() {

		Object o;


		while( true ){
			try {
				clientsToDelete = new Vector<ClientTalker>();
				o = broadcast.take();
				if( o!=null )
					for( ClientTalker ct : clients ){
						try {

							if ( o instanceof ControlProtocol
									&& ( (ControlProtocol)o).getDestinationUser()!=null
									&& ( ((ControlProtocol)o).getDestinationUser().compareTo(ct.getUserName()) ) != 0 ) continue;
							else {
								System.out.println("Broadcastuje objekt do " + ct.getUserName() );
								ct.push( o );
							}

						} catch (Exception e) {
							System.err.println("Brak połączenia z "+ct.getUserName()+", rozłączanie.");
							clientsToDelete.add( ct );
						}
					}
				for( ClientTalker ct : clientsToDelete ) {
					ct.close();
					clients.remove(ct);
				}

			} catch (Exception e) {
				System.err.println("** Błąd broadcastera - " + e.getMessage() + " **" );
				e.printStackTrace();
				System.exit(0);
			}
		}


	}
}
