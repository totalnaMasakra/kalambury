package kalambury_srv;

import java.io.FileReader;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import kalambury_cli.NetworkDrawPanelInternetPackage;

class MasterOfCeremony extends Thread {

	private Vector<ClientTalker> clients;
	protected LinkedBlockingQueue<Object> broadcast;
	private LinkedBlockingQueue<mcSession> session;
	private String[] words;
	private ClientTalker actualDrawingUser;

	public MasterOfCeremony( Vector<ClientTalker> arg0, LinkedBlockingQueue<Object> arg1, LinkedBlockingQueue<mcSession> arg2 ) {
		clients = arg0;
		broadcast = arg1;
		session = arg2;
	}

	private void loadWords(){
		FileReader inStream;
		int ch=0;
		String readed = "";

		try {
			//System.out.println(System.getProperty("user.dir"));
			inStream = new FileReader( "bin/kalambury_srv/words.txt" );
			while( (ch = inStream.read()) != -1 ) readed += (char)ch ;
			words = readed.split(",");
			inStream.close();
		} catch (Exception e ) { System.err.println( "Błąd przy wczytywaniu słów - " + e.getMessage() ); }

	}
	private String chooseWord(){
		return words[  (int)(Math.random()*1000) % words.length  ];
	}

	public void run(){

		String s, word, stats;
		mcSession mc = null;
		ControlProtocol ct = null;

		loadWords();

		while(true){
			try {

				if( clients.size() < 2 ) try {
					sleep(3000);
					mc = null;
					broadcast.put( new ControlProtocol( ControlProtocol.WAITING ) );
					broadcast.put( "Czekanie na co najmniej 2 użytkowników\n" );
					continue;
				} catch(Exception e){}

				if( mc==null ) {
					int i = (int)(Math.random()*1000) % clients.size();
					mc = new mcSession( clients.get(i).getUserName() , clients.get(i), null);
				}

				s = "";
				word = chooseWord();

				actualDrawingUser = mc.ct;
				broadcast.put( new ControlProtocol( ControlProtocol.PROBLEM ) );
				broadcast.put( new ControlProtocol( ControlProtocol.MASTER , actualDrawingUser.getUserName(), word ) );

				while( word.compareToIgnoreCase( s )!=0 && clients.size() >= 2 ){
					mc = session.poll(1500, TimeUnit.MILLISECONDS);

					if( clients.indexOf(actualDrawingUser) == -1 ) {
						mc=null;
						break;
					}
					if( mc == null) continue;
					else if( mc.object instanceof NetworkDrawPanelInternetPackage ) broadcast.put(mc.object);
					else if( mc.object instanceof String ){
						broadcast.put( mc.ct.getUserName() + ": " + (String)mc.object + "\n" );
						s = (String) mc.object;
					}
					else if( mc.object instanceof ControlProtocol ){
						ct = (ControlProtocol)mc.object;
						if( (ct.getValue() == ControlProtocol.RESTART && ct.getDestinationUser().compareTo( actualDrawingUser.getUserName() )==0)) {
							mc=null;
							break;
						}
						else if( ct.getValue() == ControlProtocol.STATS ){
							stats= "<html><table>";
							stats+="<tr><td>Gracz</td><td>Punkty</td></tr>";

							for( ClientTalker client : clients ) {
								stats+= "<tr><td>" + client.getUserName() + "</td><td>" + client.getPoints() + "</td></tr>" ;
							}

							stats+= "</table></html>";
							broadcast.put( new ControlProtocol(ControlProtocol.STATS, ct.getDestinationUser(), stats) );
						}
					}
				}

				if ( word.compareToIgnoreCase( s ) == 0 ){
					mc.ct.addPoint();
					broadcast.put( mc.ct.getUserName() + " zgadł, dostaje punkt.\n" );
					actualDrawingUser = mc.ct;
				}


			}catch (Exception e) { System.err.println("Błąd mc - " + e.getMessage() );}
		}
	}

}

class mcSession {
	public final String userName;
	public final ClientTalker ct;
	public final Object object;
	mcSession( String userName, ClientTalker ct, Object object ) { this.userName=userName; this.ct=ct; this.object=object; }
}